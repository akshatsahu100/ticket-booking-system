import  express from "express";
import userRoutes from "./routes/userRoutes";
import busRoutes from "./routes/busRoutes";
import ticketRoutes from "./routes/ticketRoutes";
import mongoose from "mongoose";
import dotenv from 'dotenv';
import { errorhandler, notFound } from "./middleware/errorHandler";
dotenv.config();

const app = express();
app.use(express.json());

//================         Handling routes      =====================//
app.use('/users', userRoutes);
app.use('/bus', busRoutes);
app.use('/ticket', ticketRoutes);
app.use(errorhandler);
app.use(notFound);


//=====================       Connections        =========================//
const port = process.env.PORT;
const db = process.env.DB;

async function mongoConnection() {
     try{
         await mongoose.connect(db || "mongodb://localhost:27017/ticket-booking");
         console.log("Connected to mongodb successfully...");
     } catch(err){
         console.log("Error occurred while connecting to MongoDB", err);
     }
 }
 app.listen(port, () => {
     console.log(`Server up and running at http://localhost:${port}...`);
});
 mongoConnection();

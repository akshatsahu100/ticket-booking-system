import mongoose from 'mongoose';

interface IUser {
     name: string;
     email: string;
     password: string;
     phone: number;
     role: string;
}
interface userModelInterface extends mongoose.Model<userInterface> {
     build(attr: IUser): userInterface;
}
export interface userInterface extends mongoose.Document {
     name: string;
     email: string;
     password: string;
     phone: number;
     role: string;
}

const userSchema = new mongoose.Schema( {
     name: {
          type: String,
          required: true,
          trim: true
     },
     email: {
          type: String,
          required: true,
          trim: true,
          unique: true,

     },
     password: {
          type: String,
          required: true
     },
     phone: {
          type: Number,
          trim: true
     },
     role: {
          type: String,
          required: true,
          lowercase: true,
          default: 'user'
     }
});

userSchema.virtual('tickets', {
     ref: 'ticket',
     localField: '_id',
     foreignField: 'userid'
})

const user = mongoose.model<userInterface, userModelInterface> ('user', userSchema);
export {user};
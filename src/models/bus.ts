import mongoose from 'mongoose';

interface Ibus {
     source : string;
     destination : string;
     totalSeats : number;
     seats : number[];
}

interface busModelInterface extends mongoose.Model<busInterface> {
     build(attr: Ibus): busInterface;
}

export interface busInterface extends mongoose.Document {
    source : string;
    destination : string;
    totalSeats : number;
    seats : number[];
  }

const busSchema = new mongoose.Schema({
    source : {
        type : String,
        required : true
    },
    destination : {
        type : String,
        required : true
    },
    totalSeats : {
        type : Number,
        required : true
    },
    seats : {
        type : [Number]
    }
})

const bus = mongoose.model<busInterface, busModelInterface>('bus',busSchema);

export {bus};

import mongoose from 'mongoose';

interface Iticket {
     name : string;
     busId : mongoose.Schema.Types.ObjectId;
     seatNumber : number;
     status : string;
     userId : mongoose.Schema.Types.ObjectId; 
}

interface ticketModelInterface extends mongoose.Model<ticketInterface> {
     build(attr: Iticket) : ticketInterface;
} 

export interface ticketInterface extends mongoose.Document {
    name : string,
    busId : mongoose.Schema.Types.ObjectId,
    seatNumber : number,
    status : string,
    userId : mongoose.Schema.Types.ObjectId
  }

const ticketSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true
    },
    busId : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'bus',
        required : true
    },
    seatNumber : {
        type : Number,
        required : true
    },
    status : {
        type : String
    },
    userId : {
        type : mongoose.Schema.Types.ObjectId,
        required : true,
        ref : 'User'
    }
})
const ticket  = mongoose.model<ticketInterface, ticketModelInterface>('ticket', ticketSchema);
export {ticket};

import {ticket, ticketInterface} from '../models/ticket';
import {bus, busInterface} from '../models/bus';
import {userInterface} from '../models/user';
import { ObjectId } from 'mongoose';


export const bookTicket = async (user: userInterface, bus: busInterface, seat: number) => {
     try{
          const ticketdata = {
               name: user.name,
               busId: bus._id,
               seatNumber: seat,
               status: 'booked',
               userId: user.id
          }
          const newticket = new ticket(ticketdata);
          bus.seats = bookseat(bus.seats, seat);
          await bus.save();
          await newticket.save();
          return newticket;
     }
     catch(err:any) {
          throw new Error(err);
     }
}

export const cancelTicket = async (targetticket: ticketInterface) => {
     try{
          const targetbus = await bus.findOne({_id: targetticket.busId});
          if(!targetbus){
               await targetticket.delete();
               return true;
          }
          targetbus.seats.push(targetticket.seatNumber);
          await targetbus.save();
          await targetticket.delete();
          return true;
     }
     catch(err: any){
          throw new Error(err);
     }
}

export const deleteAllTickets = async () => {
     try {
          await ticket.remove({});
          return true;
     }
     catch( err: any){
          throw new Error(err);
     }
}
export const findAllTickets = async () => {
     try{
          const alltickets = await ticket.find({});
          return alltickets;
     }
     catch(err: any){
          throw new Error(err);
     }
}
export const findUserTickets = async (id: ObjectId) => {
     try{
          const alltickets = await ticket.find({userId: id});
          return alltickets;
     }
     catch(err: any){
          throw new Error(err);
     }
}
export const findSingleTicket = async (id: ObjectId | string) => {
     try{
          const targetTicket = await ticket.findOne({_id : id});
          return targetTicket;
     }
     catch(err: any){
          throw new Error(err);
     }
}
export const checkavailability = (arr: number[], target: number) : boolean => {
     for(let i = 0; i < arr.length; i++){
          if(arr[i] == target)
               return true;
     }
     return false;
}
export const bookseat = (arr: number[], target: number) : number [] => {
     arr = arr.filter((s):boolean=> {return s!=target});
     return arr;
}
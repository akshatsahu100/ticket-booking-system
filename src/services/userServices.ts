import * as bcrypt from "bcrypt";
import { user, userInterface } from '../models/user';
import { getJWTToken } from '../middleware/auth';
import { ObjectId } from 'mongoose';



export const validateUser = async (targetEmail: string, targetPassword: string) => {
     try{
          const targetUser : (userInterface & {_id: any}) | null = await user.findOne({email : targetEmail});
          if(!targetUser)
               return false;
          if(await bcrypt.compare(targetPassword, targetUser.password)){
               const payload = {
                    id: targetUser._id,
                    name: targetUser.name,
                    email: targetUser.email,
                    role: targetUser.role
               }
               const token : string = getJWTToken(payload);
               return {accessToken: token, user: payload};
          }
          else{
               return false;
          }     
     }
     catch(err: any){
          throw new Error(err);
     }


}

export const createUser = async (reqBody : userInterface, role: string) => {
     try {
          const hashedpassword:string = await bcrypt.hash(reqBody.password, 10);
          const userData = {
               name: reqBody.name,
               email: reqBody.email,
               password: hashedpassword,
               phone: reqBody.phone,
               role: role
          }
          const newuser = new user(userData);
          await newuser.save();
          return newuser;
     }
     catch(err : any) {
          throw new Error(err);
     }
}

export const findUser = async (id: ObjectId | string) => {
     try{
          const founduser = await user.findOne({_id : id});
          return founduser;
     }
     catch(err: any){
          throw new Error(err);
     }
}
export const findUserByEmail = async (targetEmail : string) => {
     try{
          const targetUser = await user.findOne({email : targetEmail});
          return targetUser;
     }
     catch(err: any) {
          throw new Error(err);
     }
}
export const findAllUsers = async () => {
     try{
          const allusers = await user.find({});
          return allusers;
     }
     catch(err: any){
          throw new Error(err);
     }
}
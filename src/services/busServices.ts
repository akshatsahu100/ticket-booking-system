import {bus, busInterface } from '../models/bus';
import {ticket} from '../models/ticket';
import { ObjectId } from "mongoose";

export const createbus = async (reqBody: busInterface) => {
     try{
          const seatarray: number[] = [];
          
          const busdata = new bus({
               source: reqBody.source,
               destination: reqBody.destination,
               totalSeats: reqBody.totalSeats,
               seats: seatarray
          });
          for(let i = 0; i < reqBody.totalSeats; i++)
               busdata.seats[i] = i+1;
          
          const newbus = new bus(busdata); 
          await newbus.save();
          return newbus;
     }
     catch(err: any) {
          throw new Error(err);
     }
}

export const findAllBuses = async () => {
     try{
          const allbuses = await bus.find({});
          return allbuses;
     }
     catch(err: any){
          throw new Error(err);
     }
}

export const findSingleBus = async (id: ObjectId | string) => {
     try{
          const targetBus = await bus.findOne({_id: id});
          return targetBus;
     }
     catch(err: any){
          throw new Error(err);
     }
}

export const resetAllBuses = async () => {
     try{
          const allbuses = bus.find({});
          (await allbuses).forEach( async (bus) => {
               for(let i = 0; i < bus.totalSeats; i++)
                    bus.seats[i] = i + 1;
               await bus.save();
          });
          return;
     }
     catch(err: any){
          throw new Error(err);
     }
}

export const resetBus = async (id: ObjectId | string) => {
     try{
          await ticket.remove({busId: id});
          const targetbus = await bus.findOne({_id: id});
          if(!targetbus)
               return false;
          for(let i = 0; i < targetbus.totalSeats; i++)
               targetbus.seats[i] = i+1;
          await targetbus.save();
          return true;
     }
     catch(err:any){
          throw new Error(err);
     }
}
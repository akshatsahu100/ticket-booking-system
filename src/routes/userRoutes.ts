import express from "express"
import * as userControllers from '../controllers/userControllers';
import * as authfunctions from '../middleware/auth';
const router = express.Router();


//===================       POST requests          ======================//
router.post('/login-user', userControllers.loginUser);
router.post('/login-admin', userControllers.loginAdmin);
router.post('/register-user', userControllers.registerUser); 
router.post('/register-admin', userControllers.registerAdmin); 

//===================        GET requests           ========================//
router.get('/', authfunctions.userAuth, authfunctions.adminAuth, userControllers.getUsers); 
router.get('/:id', authfunctions.userAuth, userControllers.getSingleUser); 

export default router;
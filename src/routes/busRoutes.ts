import express from "express"
import * as busControllers from '../controllers/busControllers';
import * as authfunctions from '../middleware/auth';

const router = express.Router();

// ==================         Post Requests         =================== //
router.post('/add-bus',authfunctions.userAuth, authfunctions.adminAuth, busControllers.addBus);
router.post('/reset',authfunctions.userAuth, authfunctions.adminAuth, busControllers.resetAllBuses); 
router.post('/reset/:id',authfunctions.userAuth, authfunctions.adminAuth, busControllers.resetBus); 

// ==================          Get Requests         =================== //
router.get('/',authfunctions.userAuth, busControllers.getAllbuses);
router.get('/:id',authfunctions.userAuth, busControllers.getSingleBus);
router.get('/:id/available-seats',authfunctions.userAuth, busControllers.getBusSeats);

export default router;
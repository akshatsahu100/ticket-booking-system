import express from "express";
import * as ticketControllers from '../controllers/ticketControllers';
import * as authfunctions from '../middleware/auth';
const router = express.Router();

// ==============          POST Requests          ================== //
router.post('/book',authfunctions.userAuth, ticketControllers.bookTicket);
router.post('/cancel/:id',authfunctions.userAuth, ticketControllers.cancelTicket);
router.post('/cancel',authfunctions.userAuth, authfunctions.adminAuth, ticketControllers.cancelAllTickets);

// =============           GET Requests           ==================== //
router.get('/',authfunctions.userAuth, authfunctions.adminAuth, ticketControllers.getAllTickets);
router.get('/my-tickets',authfunctions.userAuth, ticketControllers.getAllMyTickets);
router.get('/:id',authfunctions.userAuth, ticketControllers.getSingleTicket);


export default router;

import { NextFunction, Request, Response } from "express";
import * as ticketServices from "../services/ticketServices";
import { resetAllBuses , findSingleBus} from "../services/busServices";
import { BadRequest, Forbidden, InternalServerError} from "@curveball/http-errors";


export const bookTicket = async (req: Request, res: Response, next: NextFunction) => {
     try{
          const targetbus = await findSingleBus(req.body.busid);
          if(!targetbus) 
               throw new BadRequest('Bus does not exist');
          const seatavailable: boolean = await ticketServices.checkavailability(targetbus.seats, req.body.seatNumber);
          if(!seatavailable)
               throw new BadRequest('Seat number either booked or invalid');
          const newticket = await ticketServices.bookTicket(req.body.user, targetbus, req.body.seatNumber);
          return res.send(newticket).status(201);
     }
     catch(err){
          next(err);
     }
}

export const getAllTickets = async (req: Request, res: Response, next: NextFunction) => {
     try{
          const result = await ticketServices.findAllTickets();
          return res.json(result).status(200);
     }
     catch(err){
          next(err);
     }
}

export const getAllMyTickets = async (req: Request, res: Response, next: NextFunction) => {
     try{
          const tickets = await ticketServices.findUserTickets(req.body.user.id);
          return res.send(tickets).status(200);
     }
     catch(err){
          next(err);
     }
}

export const getSingleTicket = async (req: Request, res: Response, next: NextFunction) => {
     try{
          const targetticket = await ticketServices.findSingleTicket(req.params.id);
          if(!targetticket)
               throw new BadRequest("ticket doesn't exist");
          if(req.body.user.role === 'admin' || req.body.user.id == targetticket.userId)
               return res.send(targetticket).status(200);
          else
               throw new Forbidden('Unauthorized request');
     }
     catch(err){
          next(err);
     }
}

export const cancelTicket = async (req: Request, res: Response, next: NextFunction) => {
     try {
          const targetticket = await ticketServices.findSingleTicket(req.params.id);
          if(!targetticket)
               throw new BadRequest('Ticket number invalid'); 
          if(req.body.user.role === 'admin' || req.body.user.id == targetticket.userId){
               const deletion = await ticketServices.cancelTicket(targetticket);
               if(!deletion)
                    throw new InternalServerError('Encountered some error');
               return res.send('Ticket cancelled successfully').status(200);
          }
          else
               throw new Forbidden('Unauthorized cancellation');
     }
     catch (err){
          next(err);
     }
}

export const cancelAllTickets = async (req: Request, res: Response, next: NextFunction) => {
     try{
          await ticketServices.deleteAllTickets();
          await resetAllBuses();
          return res.send('All tickets deleted and buses reset').status(200);
     }
     catch(err){
          next(err);
     }
}


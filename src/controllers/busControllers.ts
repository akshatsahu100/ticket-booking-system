import { NextFunction, Request, Response } from "express";
import * as busServices from '../services/busServices';
import { deleteAllTickets } from '../services/ticketServices';
import { BadRequest } from "@curveball/http-errors";



export const addBus = async (req: Request, res: Response, next: NextFunction) => {
     try{
          const newbus = await busServices.createbus(req.body);
          return res.send(newbus).status(201);
     }
     catch(err){
          next(err);
     }
}


export const getAllbuses = async (req: Request, res: Response, next: NextFunction) => {
     try{
          const result = await busServices.findAllBuses();
          return res.json(result).status(200);
     }
     catch(err){
          next(err);
     }
}

export const getSingleBus = async (req: Request, res: Response, next: NextFunction) => {
     try{
          const targetbus = await busServices.findSingleBus(req.params.id);
          if(!targetbus)
               throw new BadRequest('Bus id invalid');
          return res.send(targetbus);
     }
     catch(err){
          next(err);
     }
}
export const getBusSeats = async (req: Request, res: Response, next: NextFunction) => {
     try{
          const targetbus = await busServices.findSingleBus(req.params.id);
          if(!targetbus)
               throw new BadRequest('Bus id invalid');

          return res.json({
               totalSeats: targetbus.totalSeats,
               availableSeats : targetbus.seats
          }).status(200);
     }
     catch(err){
          next(err);
     }
}

export const resetBus = async (req: Request, res: Response, next: NextFunction) => {
     try{
          await busServices.resetBus(req.params.id);
          return res.send('Bus resetted successfully and corresponding bookings cancelled').status(200);
     }
     catch(err){
          next(err);
     }
}

export const resetAllBuses = async (req: Request, res: Response, next: NextFunction) => {
     try{
          await deleteAllTickets();
          await busServices.resetAllBuses();
          return res.send('All buses reset and bookings cancelled').status(200);
     }
     catch(err){
          next(err);
     }
}
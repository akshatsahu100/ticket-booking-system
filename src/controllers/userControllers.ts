import { NextFunction, Request, Response } from "express";
import * as userServices from '../services/userServices';
import {findUserTickets} from '../services/ticketServices';
import { BadRequest, Forbidden, Unauthorized} from "@curveball/http-errors";

export const loginUser = async (req: Request, res: Response, next: NextFunction) => {
     try {
          if(!req.body.password) return res.send('Please enter a password').status(401);
          const userdata = await userServices.validateUser(req.body.email, req.body.password);
          if(!userdata)
               throw new Unauthorized('Invalid email or password entered');
          const tickets = await findUserTickets(userdata.user.id);
          const alldata = {
               userdata: userdata,
               userTickets: tickets
          }
          return res.json(alldata).status(200);
     }
     catch (err) {
          next(err);     
     }
};

export const loginAdmin = async (req: Request, res: Response, next: NextFunction) => {
     try {
          if(!req.body.password) 
               throw new BadRequest('Please enter a password');
          const userdata = await userServices.validateUser(req.body.email, req.body.password);
          if(!userdata)
               throw new Unauthorized('Invalid email or password entered');

          if(userdata.user && userdata.user.role !== 'admin')
               throw new Forbidden('Login forbidden');
          const tickets = await findUserTickets(userdata.user.id);
          const alldata = {
               userdata: userdata,
               userTickets: tickets
          }
          return res.send(alldata).status(200);
     }
     catch (err) {
          next(err);     
     }
};

export const registerUser = async (req: Request, res: Response, next: NextFunction) => {
     try {
          if(req.body.password !== req.body.confirmpassword)
               throw new BadRequest('Passwords do not match');
          const newuser = userServices.createUser(req.body, 'user');
          if(!newuser)
               throw new BadRequest("Couldn't create user.");
          return res.status(201).send('User registration successful. You can login now').json(newuser);
     }
     catch (err) {
          next(err);
     }
};

export const registerAdmin = async (req: Request, res: Response, next: NextFunction) => {
     try {
          if(req.body.password !== req.body.confirmpassword)
               throw new BadRequest('Passwords do not match');
          const newuser = userServices.createUser(req.body, 'admin');
          if(!newuser)
               throw new BadRequest("Couldn't create admin user.");
          return res.status(201).send('Admin user registration successful. You can login now').json(newuser);
     }
     catch (err) {
          next(err);
     }
};

export const getUsers = async (req: Request, res: Response, next: NextFunction) => {
     try {
          const result = await userServices.findAllUsers();
          return res.json(result).status(200);
     }
     catch (err) {
          next(err);
     }
};

export const getSingleUser = async (req: Request, res: Response, next: NextFunction) => {
     try {
          const requestingUser = await userServices.findUser(req.body.user.id);
          const targetUser = await userServices.findUser(req.params.id);
          if(!requestingUser) 
               throw new BadRequest('Your user access not verified');
          if((requestingUser.role === 'admin') || (targetUser && targetUser.email === requestingUser.email))
               return res.send(targetUser).status(200);
          else
               throw new Forbidden('Access Forbidden');
     }
     catch (err) {
          next(err);
     }
};
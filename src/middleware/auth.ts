import { NextFunction, Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';
import { Forbidden, Unauthorized } from '@curveball/http-errors';
dotenv.config();

export const getJWTToken = (payload: any) => {
     const secretKey = process.env.JWT_SECRET_KEY;
     const accessToken = jwt.sign(payload, secretKey || "secret", {
          expiresIn: process.env.JWT_EXPIRES_IN
     });
     return accessToken;
}

export const userAuth = async (req: Request, res: Response, next: NextFunction) => {
     try{
          const token = req.headers["authorization"] && req.headers["authorization"]?.split(' ')[1];
          if(!token) 
               throw new Unauthorized('Please Login to continue');

          jwt.verify(token, process.env.JWT_SECRET_KEY || "", (err : any, user: any) => {
               if(err) throw new Forbidden('You do not have access');
               req.body.user = user;
               next()
          });
     }
     catch(err: any) {
          next(err);
     }
}

export const adminAuth = async (req: Request, res: Response, next: NextFunction) => {
     try{
          if(req.body.user.role !== 'admin')
               throw new Forbidden('Access Forbidden');
          next();
     }
     catch(err){
          next(err);
     }
}
import { Request, Response, NextFunction } from "express";
import { NotFound } from "@curveball/http-errors/dist";

export const errorhandler = async (err : any, req: Request, res: Response, next: NextFunction) =>{
     res.status = err.httpStatus || err.statusCode || err.status || 500;
     return res.json(err);
}

export const notFound = async (err : any, req: Request, res: Response, next: NextFunction) => {
     err = new NotFound('Page not found');
     return res.send(err).status(404);
}
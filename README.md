**PostMan Collection**

https://www.getpostman.com/collections/748919064c057d49ff6a

**Terminal Commands**  
- Run Project : `npm run dev`
- Build Project: `npm run build`
- Lint Project: `npm run lint`


**API ENDPOINTS**

User routes: 
1) `user/login-user` : Login a normal user
2) `user/login-admin` : Login an admin user
3) `user/register-user` : Register a normal user
4) `user/register-admin` : Register a normal admin
5) `user/` : Get all users
6) `user/:id` : Get a particular user

Bus Routes:
1) `bus/add-bus` : Add a new bus
2) `bus/reset` : Reset all seats in all buses, thereby cancellling all corresponfing tickets
3) `bus/reset/:id` : Reset all seats in a particular buses, thereby cancellling all corresponfing tickets
4) `bus/` : Get all buses details
5) `bus/:id` : Get a particular bus details
6) `bus/:id/available-seats` : Get the list of available seats in a particular bus

Ticket Routes:
1) `ticket/book` : Book a ticket
2) `ticket/cancel` : Cancel all bookings and resetting all buses
3) `ticket/cancel/:id` : Cancel a particular ticket
4) `ticket/` : Get all tickets booked till date
5) `ticket/:id` : Get a particular ticket detail
6) `ticket/my-tickets`: Get tickets for a particular user
